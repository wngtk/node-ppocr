"use strict";

const { spawn } = require("node:child_process");
const { joinSafe, normalizeTrim } = require("upath");

class PaddleOCR {
	/** @param {string} binPath - Path of paddleOCR infer binaries. */
	constructor(binPath) {
		if (binPath) {
			this.paddlePath = normalizeTrim(binPath);
		} else if (process.platform === "win32") {
			this.paddlePath = joinSafe(__dirname, "lib", "win64");
		} else {
			throw new Error(
				`${process.platform} paddleOCR infer binaries are not provided, please pass the installation directory as a parameter to the PaddleOCR instance.`
			);
		}
	}

	/**
	 * @author wngtk
	 * @description Infer text on `file`.
	 * @param {string} file - filepath of the image file to read.
	 * @param {object} options - Object containing options to pass to binary.
	 * @param {string} [options.det_model] - det_model path.
	 * @param {string} [options.rec_model] - rec_model path.
	 * @param {string} [options.keys] - ppocr_keys_v1.txt path.
	 * @returns {Promise<string>} - stringified JSON.
	 */
	async infer(file, options = {}) {
		const args = [
			options["det_model"] ||
				joinSafe(this.paddlePath, "ch_PP-OCRv4_det_server_infer"),
			options["rec_model"] ||
				joinSafe(this.paddlePath, "ch_PP-OCRv4_rec_infer"),
			options["keys"] || joinSafe(this.paddlePath, "ppocr_keys_v1.txt"),
			file,
			"1",
		];

		return new Promise((resolve, reject) => {
			const child = spawn(joinSafe(this.paddlePath, "infer"), args);

			let stdOut = "";
			let stdErr = "";

			child.stdout.on("data", (data) => {
				stdOut += data;
			});

			child.stderr.on("data", (data) => {
				stdErr += data;
			});

			child.on("close", (code) => {
				if (stdOut !== "" && code === 0) {
					resolve(stdOut.trim());
				} else if (stdErr !== "") {
					reject(new Error(stdErr.trim()));
				} else {
					reject(
						new Error(
							`infer ${args.join(" ")} exited with code ${code}`
						)
					);
				}
			});
		});
	}
}

module.exports.PaddleOCR = PaddleOCR;
