const { PaddleOCR } = require(".");

const testDirectory = `${__dirname}/../test_resource/`;
const file = `${testDirectory}image.jpg`;

describe("infer function", () => {
	it("infer jpg image", async () => {
		const paddle = new PaddleOCR();
		const res = await paddle.infer(file);
        expect(res).toBeTruthy()
	});
}, 120000);
