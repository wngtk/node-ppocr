# node-ppocr

> Asynchronous node.js wrapper for the PaddleOCR C++ deployed.

## Overview

The `node-ppocr` module provides an asynchronous node.js wrapper around said general PaddleOCR for easier use.

## Installation

Install using `npm`:

```bash
npm i https://gitlab.com/wngtk/node-ppocr.git
```

### Linux and macOS/Darwin support

Windows binaries are provided with this repository.
For Linux/macOS users, you can compile ppocr by yourself.

## API

```js
import { PaddleOCR } from "@wngtk/node-ppocr";
```

## Examples

```js
import { PaddleOCR } from "@wngtk/node-ppocr";

const paddle = new PaddleOCR();
const res = await paddle.infer("./image.jpg");
console.log(JSON.parse(res));
```
