export class PaddleOCR {
    /** @param {string} binPath - Path of paddleOCR infer binaries. */
    constructor(binPath: string);
    paddlePath: string;
    /**
     * @author wngtk
     * @description Infer text on `file`.
     * @param {string} file - filepath of the image file to read.
     * @param {object} options - Object containing options to pass to binary.
     * @param {string} [options.det_model] - det_model path.
     * @param {string} [options.rec_model] - rec_model path.
     * @param {string} [options.keys] - ppocr_keys_v1.txt path.
     * @returns {Promise<string>} - stringified JSON.
     */
    infer(file: string, options?: {
        det_model?: string;
        rec_model?: string;
        keys?: string;
    }): Promise<string>;
}
